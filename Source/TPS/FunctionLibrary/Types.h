// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum  class EMovementState : uint8
{
	Aim_State UMETA(DisplayName="Aim State"),
	Walk_State UMETA(DisplayName="Walk State"),
	AimWalk_State UMETA(DisplayName="AimWalk State"),
	Run_State UMETA(DisplayName="Run State"),
	SrintRun_State UMETA(DisplayName="SprintRun State")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{ 
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="MoveMent")
		float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="MoveMent")
		float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MoveMent")
		float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="MoveMent")
		float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category="MoveMent")
		float SprintRunSpeedRun = 800.0f;

};

UCLASS()
class TPS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};
